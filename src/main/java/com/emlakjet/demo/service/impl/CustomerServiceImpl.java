package com.emlakjet.demo.service.impl;

import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.repository.CustomerRepository;
import com.emlakjet.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getCustomerByID(Long custId) {
        return customerRepository.findById(custId).orElse(null);
    }

    @Override
    public Customer getCustomerByNameAndEmail(String firstName, String lastName, String email) {
        return customerRepository.findByNameAndMail(firstName, lastName, email).orElse(null);
    }
    @Override
    public Customer saveCustomer(RequestDto requestDto){
        Customer customerByNameAndEmail = getCustomerByNameAndEmail(requestDto.getFirstName(), requestDto.getLastName(),requestDto.getEmail());
        if (customerByNameAndEmail!=null){
            return customerByNameAndEmail;
        }
        return customerRepository.save(Customer.builder().firstName(requestDto.getFirstName())
                .lastName(requestDto.getLastName()).email(requestDto.getEmail()).build());
    }

}
