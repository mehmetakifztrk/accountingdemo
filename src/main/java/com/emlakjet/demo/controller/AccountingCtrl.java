package com.emlakjet.demo.controller;

import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.BillMapper;
import com.emlakjet.demo.mapper.dto.CustomerDto;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.mapper.dto.ResponseDto;
import com.emlakjet.demo.service.BillService;
import com.emlakjet.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
@Slf4j
public class AccountingCtrl {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private BillService billService;
    @Autowired
    private BillMapper billMapper;

    @PostMapping("getBills")
    public ResponseEntity<ResponseDto> getBills(@RequestBody CustomerDto customerDto) {
        Customer customer = customerService.getCustomerByNameAndEmail(customerDto.getFirstName(),
                customerDto.getLastName(), customerDto.getEmail());
        ResponseDto responseDto = billService.getBills(customer);
        if (responseDto == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseDto);
    }

    @PostMapping("saveBill")
    public ResponseEntity<ResponseDto> saveBill(@RequestBody RequestDto requestDto) {

        Customer customer = customerService.saveCustomer(requestDto);
        ResponseDto responseDto = billService.saveBill(requestDto, customer);
        if (responseDto == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        if (responseDto.getAcceptedBills() != null && !responseDto.getAcceptedBills().isEmpty()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDto);
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(responseDto);
    }

}
