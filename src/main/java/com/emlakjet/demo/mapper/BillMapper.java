package com.emlakjet.demo.mapper;


import com.emlakjet.demo.entity.Bill;
import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.dto.BillDto;
import com.emlakjet.demo.mapper.dto.CustomerDto;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.mapper.dto.ResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BillMapper {

    List<BillDto> toDtos(List<Bill> bill);

    BillDto toDto(Bill bill);

    void  fillBillEntity(RequestDto requestDto, @MappingTarget Bill bill);

}
