package com.emlakjet.demo.repository;

import com.emlakjet.demo.entity.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {

    @Query(value = "SELECT * FROM BILL y WHERE y.CUST_ID = :custid ", nativeQuery = true)
    List<Bill> findByCustId(@Param("custid") Long custId);

    @Query(value = "SELECT sum(y.AMOUNT) FROM BILL y WHERE y.CUST_ID = :custid AND y.ACCEPTED = TRUE", nativeQuery = true)
    Long findSuccessfulPurchaseTotalAmount(@Param("custid") Long custId);
}
