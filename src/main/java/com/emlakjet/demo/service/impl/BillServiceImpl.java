package com.emlakjet.demo.service.impl;

import com.emlakjet.demo.entity.Bill;
import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.BillMapper;
import com.emlakjet.demo.mapper.dto.BillDto;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.mapper.dto.ResponseDto;
import com.emlakjet.demo.repository.BillRepository;
import com.emlakjet.demo.service.BillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Slf4j
public class BillServiceImpl implements BillService {

    @Value("${limit}")
    private String limit;

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private BillMapper billMapper;

    @Override
    public ResponseDto getBills(Customer customer) {
        ResponseDto responseDto = ResponseDto.builder().firstName(customer.getFirstName()).lastName(customer.getLastName())
                .email(customer.getEmail()).acceptedBills(new ArrayList<>())
                .notAcceptableBills(new ArrayList<>()).build();
        List<Bill> billList = billRepository.findByCustId(customer.getId());
        billMapper.toDtos(billList).stream().forEach(x->{
            if(x.isAccepted()){
                responseDto.getAcceptedBills().add(x);
            }else {
                responseDto.getNotAcceptableBills().add(x);
            }
        });
        return responseDto;
    }

    @Override
    public Long getsuccessfulTotalAmount(Long custId) {
        Long successfulPurchaseTotalAmount = billRepository.findSuccessfulPurchaseTotalAmount(custId);
        if (successfulPurchaseTotalAmount != null) {
            return successfulPurchaseTotalAmount;
        }
        return 0L;
    }

    @Override
    public ResponseDto saveBill(RequestDto requestDto, Customer customer) {

        BillDto dto = null;
        ResponseDto responseDto = null;
        try {
            Long amountLimit = new Long(limit);
            Bill bill = Bill.builder().build();
            billMapper.fillBillEntity(requestDto, bill);
            bill.setCustId(customer.getId());
            long sum = Long.sum(getsuccessfulTotalAmount(bill.getCustId()), bill.getAmount());
            if (amountLimit.compareTo(sum) > -1) {
                bill.setAccepted(true);
            } else {
                bill.setAccepted(false);
            }
            Bill save = billRepository.save(bill);
            dto = billMapper.toDto(save);
            responseDto = ResponseDto.builder().firstName(customer.getFirstName())
                    .lastName(customer.getLastName()).email(customer.getEmail()).build();
            if (dto.isAccepted()) {
                responseDto.setAcceptedBills(Collections.singletonList(dto));
            } else {
                responseDto.setNotAcceptableBills(Collections.singletonList(dto));
            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return responseDto;
    }
}
