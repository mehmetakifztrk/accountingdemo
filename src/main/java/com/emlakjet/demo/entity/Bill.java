package com.emlakjet.demo.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BILL", indexes = {
        @Index(name = "CUST_ID_INDX", columnList = "CUST_ID")
})
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BILL_SEQ")
    @SequenceGenerator(name = "BILL_SEQ", sequenceName = "BILL_SEQ", initialValue = 100)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "AMOUNT")
    private Long amount;

    @Column(name = "ACCEPTED")
    private boolean accepted;

    @Column(name = "BILL_NO")
    private String billNo;

    @Column(name = "CUST_ID")
    private Long custId;

}
