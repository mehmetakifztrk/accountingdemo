package com.emlakjet.demo.mapper.dto;


import lombok.*;

@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BillDto {

    private String productName;

    private Long amount;

    private boolean accepted;

    private String billNo;

    private Long custId;

}
