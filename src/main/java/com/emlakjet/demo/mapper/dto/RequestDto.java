package com.emlakjet.demo.mapper.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestDto {

    private String firstName;

    private String lastName;

    private String email;

    private String productName;

    private Long amount;

    private String billNo;
}
