package com.emlakjet.demo.service;

import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.dto.RequestDto;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {

    Customer getCustomerByID(Long custId);

    Customer getCustomerByNameAndEmail(String firstName, String lastName, String email);

    Customer saveCustomer(RequestDto requestDto);
}
