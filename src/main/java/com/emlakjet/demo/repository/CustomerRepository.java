package com.emlakjet.demo.repository;

import com.emlakjet.demo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value = "SELECT * FROM CUSTOMER y WHERE y.FIRST_NAME = :firstName AND y.LAST_NAME = :lastName AND y.EMAIL = :email", nativeQuery = true)
    Optional<Customer> findByNameAndMail(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("email") String email);

}
