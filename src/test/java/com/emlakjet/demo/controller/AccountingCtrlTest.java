package com.emlakjet.demo.controller;

import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.BillMapper;
import com.emlakjet.demo.mapper.dto.BillDto;
import com.emlakjet.demo.mapper.dto.CustomerDto;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.mapper.dto.ResponseDto;
import com.emlakjet.demo.service.BillService;
import com.emlakjet.demo.service.CustomerService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.assertThat;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class AccountingCtrlTest {

    @InjectMocks
    private AccountingCtrl accountingCtrl;
    @Mock
    private CustomerService customerService;
    @Mock
    private BillService billService;
    @Mock
    private BillMapper billMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldGetBills() {

        //Given
        CustomerDto customerDto = CustomerDto.builder().firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").build();

        Customer customer = Customer.builder().id(1L).firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").build();

        BillDto build = BillDto.builder().billNo("TR000").accepted(true).amount(50L).custId(1L)
                .productName("Mouse").build();

        BillDto build1 = build.toBuilder().accepted(false).build();

        ResponseDto responseDto = ResponseDto.builder().firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").acceptedBills(Collections.singletonList(build))
                .notAcceptableBills(Collections.singletonList(build1)).build();

        when(customerService.getCustomerByNameAndEmail("Mehmet Akif", "Öztürk","mehmetakifztrk@outlook.com")).thenReturn(customer);
        when(billService.getBills(customer)).thenReturn(responseDto);

        //When
        ResponseEntity<ResponseDto> bills = accountingCtrl.getBills(customerDto);

        //Then
        Assert.assertEquals(bills.getBody().getEmail(), responseDto.getEmail());

    }

    @Test
    void shouldSaveBill() {

        //Given
        RequestDto requestDto = RequestDto.builder().firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").billNo("TR000").amount(150L).productName("USB").build();

        Customer customer = Customer.builder().id(1L).firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").build();

        BillDto build = BillDto.builder().billNo("TR000").accepted(true).amount(50L).custId(1L)
                .productName("Mouse").build();

        BillDto build1 = build.toBuilder().accepted(false).build();

        ResponseDto responseDto = ResponseDto.builder().firstName("Mehmet Akif").lastName("Öztürk")
                .email("mehmetakifztrk@outlook.com").acceptedBills(Collections.singletonList(build))
                .notAcceptableBills(Collections.singletonList(build1)).build();

        when(customerService.saveCustomer(requestDto)).thenReturn(customer);
        when(billService.saveBill(requestDto, customer)).thenReturn(responseDto);

        //When
        ResponseEntity<ResponseDto> responseDtoResponseEntity = accountingCtrl.saveBill(requestDto);

        //Then
        Assert.assertNotNull(responseDtoResponseEntity);
    }
}
