package com.emlakjet.demo.mapper.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto {

    private String firstName;

    private String lastName;

    private String email;

    private List<BillDto> acceptedBills;

    private List<BillDto> notAcceptableBills;
}
