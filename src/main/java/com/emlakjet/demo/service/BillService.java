package com.emlakjet.demo.service;

import com.emlakjet.demo.entity.Customer;
import com.emlakjet.demo.mapper.dto.RequestDto;
import com.emlakjet.demo.mapper.dto.ResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface BillService {

    ResponseDto getBills(Customer customer);

    Long getsuccessfulTotalAmount(Long custId);

    ResponseDto saveBill(RequestDto requestDto, Customer customer);

}
